package aed;
public class SistemaCNE {
    // Completar atributos privados
    String[] partidos;
    String[] distritos;
    int[] votosPresidenciales;
    int[][] votosDiputados;
    int[] ultimaMesaPorDistrito;
    int primero; // id del primero (presidencial)
    int segundo; // id del segundo (presidencial)
    int votosTotalesPresidenciales;
    int votosTotalesDiputados;
    heapEspecial[] heapDiputados;  // TO DO: Cambiar nombre a algo más declarativo.
    int[] diputadosPorDistrito;  // Me dice la cantidad de diputados en disputa en cada distrito
    int[][] memoBancasPorDistrito;
    boolean[] memoFlag;  // Me dice si en el distrito que estoy considerando el resultado de la memo es valido

    public class Nodo{
            int partido;  // id_partido.
            int votosTotales;
            int votoCocientizado;
            int cociente;

            public Nodo(int partido, int votoCocientizado, int votosTotales, int cociente){
                this.partido=partido;
                this.votoCocientizado=votoCocientizado;
                this.votosTotales=votosTotales;
                this.cociente=cociente;
            }
        }

    public class heapEspecial{
        public Nodo[] arr;
        public int longitud;

        // OBS: |arr| es equivalente a la cantidad de elementos en nuestro heapEspecial.

        public heapEspecial(Nodo[] arr){  // O(|arr|)
            this.arr=arr;  // O(1) pasa la referencia
            this.longitud=arr.length;  // O(1)
            array2Heap(this.arr);  // O(|arr|), que en la practica por aca solo pasan arr de magnitud P entonces seria O(P)
        }

        public int hijo_izq(int i) {return 2*i;}  // O(1)

        public int hijo_der(int i) {return 2*i+1;}  // O(1)
    
        public int padre(int i) {return  i/2;}  // O(1)  // Devuelve el piso de la división.


        private void bajar(int i){  // O(Altura del heap), como el heap esta perfectamente balanceado salvo el último nivel, su altura es del orden log(|arr|)
            int masGrande = i;  // O(1)
            int hijoIzq = hijo_izq(i);  // O(1)
            int hijoDer = hijo_der(i);  // O(1)

            if (hijoIzq < this.longitud && this.arr[hijoIzq].votoCocientizado > this.arr[masGrande].votoCocientizado){    // Evaluar guarda: O(1). La rama más compleja es O(1)
                masGrande = hijoIzq;  // O(1)
            }
            if (hijoDer < this.longitud && this.arr[hijoDer].votoCocientizado > this.arr[masGrande].votoCocientizado){  // Evaluar guarda: O(1). La rama más compleja es O(1)
                masGrande = hijoDer;  // O(1)
            }
            if (masGrande != i){  // Evaluar guarda: O(1). La rama más compleja es O(?????)
                swap(i, masGrande);  // O(1)
                bajar(masGrande);  // O(?????)
            }
        }

        public void array2Heap(Nodo[] arreglo){
            for(int i=(this.longitud/2)-1; i>=0; i--){
                bajar(i);
            }
        }
        /*
        private void subir(int i){
            while (i != 0 && (this.arr[i].votoCocientizado >= this.arr[(i-1)/2].votoCocientizado)){
                Nodo temporal = this.arr[i];
                this.arr[i]=this.arr[(i-1)/2];
                this.arr[(i-1)/2]=temporal;
                i= i-1/2;
            }
        } por ahora no lo uso
        */
        
        public int desencolarYencolarSiguiente(){
            int partido=this.arr[0].partido;
            Nodo nuevo=new Nodo(partido,this.arr[0].votosTotales/(this.arr[0].cociente+1),this.arr[0].votosTotales,this.arr[0].cociente+1);
            this.arr[0]=nuevo;
            bajar(0);
            return partido;
        }

        private void swap(int i, int j){
            Nodo temporal = this.arr[j];
            this.arr[j] = this.arr[i];
            this.arr[i] = temporal;
        }

    }

    public class VotosPartido{
        private int presidente;
        private int diputados;
        VotosPartido(int presidente, int diputados){this.presidente = presidente; this.diputados = diputados;}
        public int votosPresidente(){return presidente;}
        public int votosDiputados(){return diputados;}
    } 

    public SistemaCNE(String[] nombresDistritos, int[] diputadosPorDistrito, String[] nombresPartidos, int[] ultimasMesasDistritos) {
        // La complejidad en el peor caso es O(D*P) por ser sumas de O(1) y O(D*P).  // CONSULTAR SI o(D) + O(D*P) + O(P) = O(D*P)
        this.partidos=nombresPartidos;  // O(1)
        this.distritos=nombresDistritos;  // O(1)
        this.votosPresidenciales= new int[this.partidos.length]; // O(P)
        this.votosDiputados=new int[this.distritos.length][this.partidos.length];  // O(D*P)
        this.ultimaMesaPorDistrito=ultimasMesasDistritos;  // O(1)
        this.primero=0;  // O(1)
        this.segundo=1;  // O(1)
        this.votosTotalesPresidenciales= 0;  // O(1)
        this.votosTotalesDiputados= 0;  // O(1)
        this.memoBancasPorDistrito = new int[distritos.length][partidos.length-1];  // O(D*P)
        this.memoFlag = new boolean[distritos.length];  // O(D)
        this.heapDiputados = new heapEspecial[this.distritos.length];  // O(D)
        this.diputadosPorDistrito=diputadosPorDistrito;  // O(1)
    }

    public String nombrePartido(int idPartido) {  // La complejidad en el peor caso O(1)
        return partidos[idPartido];  // O(1) porque devuelve una referencia a una array.
    }

    public String nombreDistrito(int idDistrito) {  // La complejidad en el peor caso O(1)
        return distritos[idDistrito];  // O(1) porque devuelve una referencia a una array.
    }

    public int diputadosEnDisputa(int idDistrito) {  // La complejidad en el peor caso O(1)
        return diputadosPorDistrito[idDistrito];  // O(1) porque devuelve una referencia a una array.
    }
    
    public String distritoDeMesa(int idMesa) {  // La complejidad en el peor caso O(1)
        return this.distritos[indiceDistritoMesa(idMesa)];  // O(1) porque devuelve una referencia a una array.
    }

    private int indiceDistritoMesa(int idMesa) {  // La complejidad en el peor caso O(log(D)).
        // Binary Search adaptada para este problema por su particularidad de los indices que devuelve.
        // arr ordenada crecientemente.
        // Por el requiere sabemos que elem pertenece entre 0 incluido y arr[arr.length - 1].
        int izq = -1;  // O(1)
        // Todos los elementos a la izquierda de izq incluido son < al elemento
        int der = this.ultimaMesaPorDistrito.length;  // O(1)
        // Todos los elementos a la derecha de der incluido son >= al elemento
        while (der-izq > 1) {  // la diferencia entre der-izq se va reduciendo por cada iteración del ciclo por la mitad. Por lo tanto,
            // el ciclo se va a ejecutar a lo sumo una cantidad del orden logaritmico de veces (log_2(D)). El cuerpo solamente contiene operaciones O(1)
            // Por lo tanto, su suma también es O(1). La complejidad del ciclo en el peor caso es O(log(D)).
            // El rango activo es [izq+1 hasta der-1]
            int medio = (izq + der) / 2;  // O(1)  -cuerpo del bucle-
            if (this.ultimaMesaPorDistrito[medio] < idMesa){  // Evaluar guarda: O(1). La rama más compleja es O(1)
                izq = medio;  // O(1)
            } else {
                der = medio;  // O(1)
            }
        }
        if (this.ultimaMesaPorDistrito[der] == idMesa){  // Evaluar guarda: O(1). La rama más compleja es O(1) // Notar que sin el requiere se rompe aca!
            return der + 1;  // O(1)
        } else {
            return der;  // O(1)
        }
    }

    private void actualizarPrimeroSegundo(){  // La complejidad en el peor caso O(P).
        // Sumando las complejidades tenemos suma de O(1) + O(P) = O(P).
        this.primero = 0;  // O(1)
        this.segundo = 1;  // O(1)
        for(int i=0; i < this.partidos.length; i++){  // El cuepo del for ejecuta P veces. Su cuerpo es O(1) por lo tanto la complejidad del ciclo for es O(P)
            if (votosPresidenciales[i] > votosPresidenciales[primero]){  // Evaluar guarda: O(1). La rama más compleja es O(1)
                this.segundo = this.primero;  // O(1)
                this.primero = i;  // O(1)
            } else if (votosPresidenciales[i] > votosPresidenciales[segundo] && i != primero){  // O(1). La rama más compleja es O(1)
                this.segundo = i;  // O(1)
            }
        }
    }

    private float porcentajeVotosPresidenciales(int idPartido){  // La complejidad en el peor caso O(1).
        return (this.votosPresidenciales(idPartido) * 100) / this.votosTotalesPresidenciales;  // O(1)
    }
    
    private float porcentajeVotosDiputados(int idDistrito, int idPartido){  // La complejidad en el peor caso O(1).
        return (this.votosDiputados(idPartido, idDistrito) * 100) / this.votosTotalesDiputados;  // O(1)
    }

    public void registrarMesa(int idMesa, VotosPartido[] actaMesa) {
        int indiceDistritoMesa = indiceDistritoMesa(idMesa);  
        for(int i=0; i < this.partidos.length; i++){  // por requiere sé que VotosPartido.length == this.partido.lenght
            this.votosPresidenciales[i] += actaMesa[i].votosPresidente();
            this.votosTotalesPresidenciales += actaMesa[i].votosPresidente();
            this.votosDiputados[indiceDistritoMesa][i] += actaMesa[i].votosDiputados();
            this.votosTotalesDiputados += actaMesa[i].votosDiputados();
            if (i!=this.partidos.length-1) {this.memoBancasPorDistrito[indiceDistritoMesa][i] = 0;}
        }
        actualizarPrimeroSegundo();
        this.memoFlag[indiceDistritoMesa] = false;
        Nodo[] arrDeNodos = new Nodo[this.partidos.length - 1];
        for (int i=0; i < this.partidos.length - 1; i++){
            if(porcentajeVotosDiputados(indiceDistritoMesa, i) >= 3){
                arrDeNodos[i] = new Nodo(i,votosDiputados(i, indiceDistritoMesa), votosDiputados(i, indiceDistritoMesa), 1);
            } else {
                arrDeNodos[i] = new Nodo(i,-1,-1, 1);  // No se les pueden asignar bancas porque no superan el 3%.
            }
        }
        this.heapDiputados[indiceDistritoMesa]= new heapEspecial(arrDeNodos);
    }

    public int votosPresidenciales(int idPartido){  // La complejidad en el peor caso O(1).
        return votosPresidenciales[idPartido];  // O(1)
    }

    public int votosDiputados(int idPartido, int idDistrito){  // La complejidad en el peor caso O(1).
        return votosDiputados[idDistrito][idPartido];  // O(1)
    }

    public int[] resultadosDiputados(int idDistrito){
        if (!this.memoFlag[idDistrito]){ // Sé que memoBancasPorDistrito[idDistrito] esta llena de ceros si no es valida.
            for(int i=0; i < this.diputadosPorDistrito[idDistrito]; i++){
                int partidoSuma=this.heapDiputados[idDistrito].desencolarYencolarSiguiente();
                this.memoBancasPorDistrito[idDistrito][partidoSuma] +=1;
            }
            this.memoFlag[idDistrito] = true;
        }
        return this.memoBancasPorDistrito[idDistrito];
    }

    public boolean hayBallotage(){  // La complejidad en el peor caso O(1).
        return !(porcentajeVotosPresidenciales(this.primero) >= 45 || (porcentajeVotosPresidenciales(this.primero) >= 40 && porcentajeVotosPresidenciales(this.primero) - porcentajeVotosPresidenciales(this.segundo) > 10));
        // Como porcentajeVotosPresidenciales habiamos visto que era O(1), la linea del return hace solamente operaciones O(1).
    }
}

