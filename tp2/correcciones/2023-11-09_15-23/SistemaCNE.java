package aed;
public class SistemaCNE {
    // Completar atributos privados
    String[] partidos;
    String[] distritos;
    int[] votosPresidenciales;
    int[][] votosDiputados;
    int[] ultimaMesaPorDistrito;
    int primero; // id del primero (presidencial)
    int segundo; // id del segundo (presidencial)
    int votosTotalesPresidenciales;
    int votosTotalesDiputados;
    heapEspecial[] heapDiputados;  // TO DO: Cambiar nombre a algo más declarativo.
    int[] diputadosPorDistrito;  // Me dice la cantidad de diputados en disputa en cada distrito
    int[][] memoBancasPorDistrito;
    boolean[] memoFlag;  // Me dice si en el distrito que estoy considerando el resultado de la memo es valido

    public class Nodo{
            int partido;
            int numero;
            int votosTotales;
            int iteracion;

            public Nodo(int partido, int numero, int votosTotales, int iteracion){
                this.partido=partido;
                this.numero=numero;
                this.votosTotales=votosTotales;
                this.iteracion=iteracion;
            }
        }

    public class heapEspecial{
        public Nodo[] array;
        public int longitud;

        public heapEspecial(Nodo[] array){
            this.array=array;
            this.longitud=array.length;
            array2Heap(this.array);
        }

        private void bajar(int i){
            while ((2*i+1<this.longitud)){
                Nodo temporal;
                if(2*i+2<this.longitud){
                    if(this.array[2*i+1].numero<this.array[2*i+2].numero){
                        if(this.array[i].numero<this.array[2*i+2].numero){
                            temporal=this.array[i];
                            this.array[i]=this.array[2*i+2];
                            this.array[2*i+2]=temporal;
                            i=2*i+2;
                        }else{
                            i=this.longitud -1; //es una forma de terminar el ciclo en la sig iteracion.
                        }
                    }else{
                        if(this.array[i].numero<this.array[2*i+1].numero){
                            temporal=this.array[i];
                            this.array[i]=this.array[2*i+1];
                            this.array[2*i+1]=temporal;
                            i=2*i+1;
                        }else{
                            i=this.longitud -1; 
                        }
                    }
                }else{
                    if(this.array[i].numero<this.array[2*i+1].numero){
                        temporal=this.array[i];
                        this.array[i]=this.array[2*i+1];
                        this.array[2*i+1]=temporal;
                        i=2*i+1;
                    }else{
                        i=this.longitud -1; 
                    }
                }
            }
        }

        public void array2Heap(Nodo[] arreglo){
            for(int i=(this.longitud/2)-1; i>=0; i--){
                bajar(i);
            }
        }
        /*
        private void subir(int i){
            while (i != 0 && (this.array[i].numero >= this.array[(i-1)/2].numero)){
                Nodo temporal = this.array[i];
                this.array[i]=this.array[(i-1)/2];
                this.array[(i-1)/2]=temporal;
                i= i-1/2;
            }
        } por ahora no lo uso
        */
        
        public int desencolarYencolarSiguiente(){
            int partido=this.array[0].partido;
            Nodo nuevo=new Nodo(partido,this.array[0].votosTotales/(this.array[0].iteracion+1),this.array[0].votosTotales,this.array[0].iteracion+1);
            this.array[0]=nuevo;
            bajar(0);
            return partido;
        }
    }

    public class VotosPartido{
        private int presidente;
        private int diputados;
        VotosPartido(int presidente, int diputados){this.presidente = presidente; this.diputados = diputados;}
        public int votosPresidente(){return presidente;}
        public int votosDiputados(){return diputados;}
    }

    public SistemaCNE(String[] nombresDistritos, int[] diputadosPorDistrito, String[] nombresPartidos, int[] ultimasMesasDistritos) {
        this.partidos=nombresPartidos;
        this.distritos=nombresDistritos;
        this.votosPresidenciales= new int[this.partidos.length];
        this.votosDiputados=new int[this.distritos.length][this.partidos.length];
        this.ultimaMesaPorDistrito=ultimasMesasDistritos;
        this.primero=0;
        this.segundo=1;
        this.votosTotalesPresidenciales=0;
        this.votosTotalesDiputados=0;
        this.memoBancasPorDistrito = new int[distritos.length][partidos.length-1];
        this.memoFlag = new boolean[distritos.length];
        this.heapDiputados=new heapEspecial[this.distritos.length];
        this.diputadosPorDistrito=diputadosPorDistrito;
    }

    public String nombrePartido(int idPartido) {
        return partidos[idPartido];
    }

    public String nombreDistrito(int idDistrito) {
        return distritos[idDistrito];
    }

    public int diputadosEnDisputa(int idDistrito) {
        return diputadosPorDistrito[idDistrito];
    }

    public String distritoDeMesa(int idMesa) {
        return this.distritos[indiceDistritoMesa(idMesa)];
    }

    private int indiceDistritoMesa(int idMesa) {
        // Binary Search adaptada para este problema por su particularidad de los indices que devuelve.
        // arr ordenada crecientemente.
        // Por el requiere sabemos que elem pertenece entre 0 y arr[arr.length - 1]
        int izq = -1;  // Todos los elementos a la izquierda de izq incluido son < al elemento
        int der = this.ultimaMesaPorDistrito.length;  // Todos los elementos a la derecha de der incluido son >= al elemento
        while (der-izq > 1) {  // El rango activo es [izq+1 hasta der-1]
            int medio = (izq + der) / 2;
            if (this.ultimaMesaPorDistrito[medio] < idMesa){
                izq = medio;
            } else {
                der = medio;
            }
        }
        if (this.ultimaMesaPorDistrito[der] == idMesa){  // Notar que sin el requiere se rompe aca!
            return der + 1;
        } else {
            return der;
        }
    }

    private void actualizarPrimeroSegundo(){
        this.primero = 0;
        this.segundo = 1;
        for(int i=0; i < this.partidos.length; i++){
            if (votosPresidenciales[i] > votosPresidenciales[primero]){
                this.segundo = this.primero;
                this.primero = i;
            } else if (votosPresidenciales[i] > votosPresidenciales[segundo] && i != primero){
                this.segundo = i;
            }
        }
    }

    private float porcentajeVotosPresidenciales(int idPartido){
        return (this.votosPresidenciales(idPartido) * 100) / this.votosTotalesPresidenciales;
    }
    
    private float porcentajeVotosDiputados(int idDistrito, int idPartido){
        return (this.votosDiputados(idPartido, idDistrito) * 100) / this.votosTotalesDiputados;
    }

    public void registrarMesa(int idMesa, VotosPartido[] actaMesa) {
        int indiceDistritoMesa = indiceDistritoMesa(idMesa);
        for(int i=0; i < this.partidos.length; i++){  // por requiere sé que VotosPartido.length == this.partido.lenght
            this.votosPresidenciales[i] += actaMesa[i].votosPresidente();
            this.votosTotalesPresidenciales += actaMesa[i].votosPresidente();
            this.votosDiputados[indiceDistritoMesa][i] += actaMesa[i].votosDiputados();
            this.votosTotalesDiputados += actaMesa[i].votosDiputados();
            if (i!=this.partidos.length-1) {this.memoBancasPorDistrito[indiceDistritoMesa][i] = 0;}
        }
        actualizarPrimeroSegundo();
        this.memoFlag[indiceDistritoMesa] = false;
        Nodo[] arrDeNodos = new Nodo[this.partidos.length - 1];
        for (int i=0; i < this.partidos.length - 1; i++){
            if(porcentajeVotosDiputados(indiceDistritoMesa, i) >= 3){
                arrDeNodos[i] = new Nodo(i,votosDiputados(i, indiceDistritoMesa), votosDiputados(i, indiceDistritoMesa), 1);
            } else {
                arrDeNodos[i] = new Nodo(i,-1,-1, 1);  // No se les pueden asignar bancas porque no superan el 3%.
            }
        }
        this.heapDiputados[indiceDistritoMesa]=new heapEspecial(arrDeNodos);
    }

    public int votosPresidenciales(int idPartido) {
        return votosPresidenciales[idPartido];
    }

    public int votosDiputados(int idPartido, int idDistrito) {
        return votosDiputados[idDistrito][idPartido];
    }

    public int[] resultadosDiputados(int idDistrito){
        if (!this.memoFlag[idDistrito]){ // Sé que memoBancasPorDistrito[idDistrito] esta llena de ceros si no es valida.
            for(int i=0; i < this.diputadosPorDistrito[idDistrito]; i++){
                int partidoSuma=this.heapDiputados[idDistrito].desencolarYencolarSiguiente();
                this.memoBancasPorDistrito[idDistrito][partidoSuma] +=1;
            }
            this.memoFlag[idDistrito] = true;
        }
        return this.memoBancasPorDistrito[idDistrito];
    }

    public boolean hayBallotage(){
        return !(porcentajeVotosPresidenciales(this.primero) >= 45 || (porcentajeVotosPresidenciales(this.primero) >= 40 && porcentajeVotosPresidenciales(this.primero) - porcentajeVotosPresidenciales(this.segundo) > 10));
    }
}

